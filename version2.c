/*
*  
*  Programmer: Atika Islam
*  Course: System Programming with Linux
*  myshellv1.c: 
*  main() displays a prompt, receives a string from keyboard, pass it to tokenize()
*  tokenize() allocates dynamic memory and tokenize the string and return a char**
*  main() then pass the tokenized string to execute() which calls fork and exec
*  finally main() again displays the prompt and waits for next command string
*   Limitations:
*   if user press enter without any input the program gives sigsegv 
*   if user give only spaces and press enter it gives sigsegv
*   if user press ctrl+D it give sigsegv
*   however if you give spaces and give a cmd and press enter it works
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <limits.h>
#define MAX_LEN 512
#define MAXARGS 10
#define ARGLEN 30
#define PROMPT "mscs536"
//GLobal variable
char * INPUT_FILE;
char* OUTPUT_FILE;
int PIPES;

char** splitting(char * arglist[], int no_of_pipes);
int pipe_count(char * arglist[]);
int p_index(char * arglist[], int index);
void p_execute(char **commands, int no_of_pipes);

int input_redirect(char* arglist[]);
int output_redirect(char* arglist[]);
int IO_redirect(char*, char*, int );
int execute(char* arglist[]);
char** tokenize(char* cmdline);
char* read_cmd(char*, FILE*);
int main(){
   char *cmdline;
   char** arglist;
   char ** comand_list;
   int stdin1,stdout1, no_of_pipes;

   int standard_in = dup(fileno(stdin));
   int standard_out = dup(fileno(stdout));
   char* prompt;
   char *pwd;
   char buff1[1024];
   prompt = (char*)malloc(sizeof(char)*1024);

   if((pwd = getcwd(buff1,sizeof(buff1))) !=NULL)
   {

	strcat(prompt,PROMPT);
	strcat(prompt, pwd);
	strcat(prompt, ":");

   }

   while((cmdline = read_cmd(prompt,stdin)) != NULL){
	  INPUT_FILE=NULL;
	  OUTPUT_FILE=NULL;
	   stdin1 = 0; stdout1=0; no_of_pipes =0;
	   if((arglist = tokenize(cmdline))!=NULL){
	
	        no_of_pipes = pipe_count(arglist);

	   	stdin1 = input_redirect(arglist);
	   	stdout1 = output_redirect(arglist);

	       	if((stdin1 ==0 && stdout1 == 0 && no_of_pipes==0))
		     execute(arglist);
		if( no_of_pipes>0)
		{
			
		
			comand_list=splitting(arglist,no_of_pipes);
			p_execute(comand_list, no_of_pipes);

     	                for(int j=0;j<MAXARGS+1;j++)
                	             free(comand_list[j]);
                       		 free(comand_list);
				 comand_list = NULL;
				 fflush(stdin);	 

                 	 

		}

	     	if(stdin1 == 1)
	     	{
		

	       		int val= IO_redirect(INPUT_FILE, OUTPUT_FILE,stdin1);

	    		if(val == 0){ printf("error"); exit(1);}
			execute(arglist);
	    	 }
	      	if(stdout1 == 2)
	     	 {
               
                	int val= IO_redirect(INPUT_FILE, OUTPUT_FILE,stdout1);
               		 if(val == 0){dup2(fileno(stdout),1); printf("error"); exit(1);};
	     		execute(arglist);
		 }


	         dup2(standard_in,0);
               	 dup2(standard_out,1);
	 
		for(int j=0; j < MAXARGS+1; j++)
		         free(arglist[j]);
         	 free(arglist);
       		 free(cmdline);
		 cmdline = NULL;
	 	fflush(stdin);
	   
	  
  	 }
	}
}


int pipe_count(char * arglist[])
{
    int count =0;
    for( int i=0;;i++){
        if(arglist[i] == NULL)
                break;
        else{

                char c = arglist[i][0];

                if(c == '|'){

                    	  count= count+1;
		}

	}
    }	
	return count;
}
//  get index of the pipe symbol
int p_index(char * arglist[], int index)
{
	for(int i= index;; i++)
	{
		if(arglist[i] == NULL)
			break;
		else{
			if(arglist[i][0] == '|')
			{    return i;}
		}
	}
	return -1;
}

// creating the commands '
char** splitting (char * arglist[], int no_of_pipes)
{

	int index = -1;
	 char** get_commands = (char**)malloc(sizeof(char**)* (MAXARGS+1));
	 for(int j=0; j < MAXARGS+1; j++){
        	   get_commands[j] = (char*)malloc(sizeof(char)* (ARGLEN)); 
     		    bzero(get_commands[j],ARGLEN);
		 }


	 int iter=0, i=0, j=0, k=0, prev_index=0;
	 char * cmd_found ="";

	while(no_of_pipes!=-1)
	{

	    cmd_found = "";


	    index = p_index(arglist, prev_index+1);
	    prev_index = index+1;


	    if(index !=-1 && arglist[iter]!=NULL)    // pipes finished
	    {
		
		j=0;

		cmd_found = arglist[iter];
		strcat(cmd_found," ");

		get_commands[i]= cmd_found;
		iter++;		
	    	while(iter < index)
	    	{

		    strcat(cmd_found, arglist[iter]);
		    iter++;
		}
		i++;
		get_commands[i] = cmd_found;

	    }
	    if(index == -1 && arglist[iter]!=NULL)
	    {
		    printf("%s\n ", arglist[iter]);
		    int spce =0;
		    while(1)
		    {
			    if(arglist[iter] == NULL)
				    break;
			    cmd_found = arglist[iter];

			    iter++;
			    if(arglist[iter] !=NULL)
				    spce++;
			    if(spce>0)
				    strcat(cmd_found, " ");

        }
		    get_commands[i] = cmd_found;

		    i++;
	    }

	  no_of_pipes--;
	  iter = prev_index;

	}
	get_commands[i]=NULL;
	i++;

	for( int j1=0;;j1++)
	{
		if(get_commands[j1] == NULL)
			break;

		//printf("%s \n" , get_commands[j1]);
	}

	return get_commands;
}

//execute_pipes
void p_execute (char **command,int no_of_pipes)
{
    char **command_argument;
    int i=0;
    PIPES =0;
    if(no_of_pipes > 0)
    {

	    int index = no_of_pipes + 1 ;

	    while(no_of_pipes>0 && PIPES<index)
	    {
	 	 command_argument = tokenize(command[0]);
		 printf("%s %s %s\n" , command_argument[0], command_argument[1], command_argument[2]);
		  int fd[2];
		  int status;
		  pipe(fd);
		  pid_t cpid = fork();
  	 
            	if(cpid ==0){  //child process
	        	command_argument = tokenize(command[1]);

      			dup2(fd[0], 0); //redirect stdin to read end of pipe
     			 close(fd[1]);
		 	 execvp(command_argument[0], command_argument);
		 	//execute(command_argument);
			command_argument = NULL;
			exit(1);
	
      	
   	    	 }
		 else if (cpid != 0){  //parent process
	
			 	command_argument = tokenize(command[0]);
		 		PIPES= PIPES+1;
		 		//printf("Pipes(parent) %d \n", PIPES);
                		 dup2(fd[1], 1); //redirect stdout to write end of pipe
                 		close(fd[0]); //not required so better close it
				 execvp(command_argument[0], command_argument);
                	 	command_argument = NULL;
			}
        	 


		no_of_pipes--;
	   }
    }
   
    printf("Pipes executes \n");
}





int IO_redirect(char* infile, char* outfile, int value)
{

   FILE * fp0;
   FILE* fp1;
   if(value == 1) // input redirection
   {
   	fp0 = fopen(infile,"r");
   	if( fp0 == NULL) {  printf("File not found. \n"); return 0;}	      
   	dup2(fileno(fp0),0);
  	fclose(fp0);
	fflush(fp0);
	return 10;
   }
   if(value ==2) // output redirection
   {
	   fp1 =fopen(outfile,"w");
	  if( fp1 == NULL) { printf("Can't find specified file. \n"); return 0;}
	  dup2(fileno(fp1),1);
	 fclose(fp1);
	 fflush(fp1);
	  return 11;
 
   }


}
// check whether the output redirection symbol comes
int output_redirect(char * arglist[])
{
    for( int i=0;;i++)
    {
        if(arglist[i] == NULL)
                break;
        else{
                char c = arglist[i][0];
                if(c == '>')
                {

                        free(arglist[i]);  // free that location where symbol is placed
                        if(arglist[i+1]!=NULL)
                                OUTPUT_FILE = arglist[i+1];
			else
				return -1;

			for(int j=i;;j++)
			{
				if(arglist[j-1]==NULL)
					break;
				else
					arglist[j]=arglist[j+2];
			
			}
			return 2;
                }
        }

    }

    return 0;

}

// check if input redirection symbol comes
int input_redirect(char *arglist[])
{
    for( int i=0;;i++)
    {

	if(arglist[i] == NULL)
		break;
	else{

		char c = arglist[i][0];

		if(c == '<')
		{
			free(arglist[i]);
			if(arglist[i+1]!=NULL)
				INPUT_FILE = arglist[i+1];
			else
				return -1;
		

			for(int j=i; ;j++)
			{
				if(arglist[j-1]==NULL)
					break;
				else
					arglist[j]= arglist[j+2];
			}
		 return 1;
		}


	}
    }
    return 0;
}

int execute(char* arglist[]){
   int status;
   int cpid = fork();
     switch(cpid){
      case -1:
         perror("fork failed");
	 exit(1);
      case 0:
	      signal(SIGINT, SIG_DFL);     
	      execvp(arglist[0], arglist);
 	      perror("Command not found...");
	      exit(1);
      default:
	      signal(SIGINT, SIG_IGN);
	      waitpid(cpid, &status, 0);
	     
 //        printf("child exited with status %d \n", status >> 8);
         return 0;
   }
}
char** tokenize(char* cmdline){
//allocate memory
  // printf("INSIDE TOKENIZE() \n");
   char** arglist = (char**)malloc(sizeof(char*)* (MAXARGS+1));
   for(int j=0; j < MAXARGS+1; j++){
	   arglist[j] = (char*)malloc(sizeof(char)* ARGLEN);
      bzero(arglist[j],ARGLEN);
    }
   if(cmdline[0] == '\0')//if user has entered nothing and pressed enter key
      return NULL;
   int argnum = 0; //slots used
   char*cp = cmdline; // pos in string
   char*start;
   int len;
   while(*cp != '\0'){
      while(*cp == ' ' || *cp == '\t') //skip leading spaces
          cp++;
      start = cp; //start of the word
      len = 1;
      //find the end of the word
      while(*++cp != '\0' && !(*cp ==' ' || *cp == '\t'))
         len++;
      strncpy(arglist[argnum], start, len);
      arglist[argnum][len] = '\0';
      argnum++;
   }
   arglist[argnum] = NULL;
   return arglist;
}      

char* read_cmd(char* prompt, FILE* fp){
   printf("%s", prompt);
  int ch; //input character
   int pos = 0; //position of character in cmdline
   char* cmdl = (char*) malloc(sizeof(char)*MAX_LEN);
   while((ch = getc(fp)) != EOF){
      // printf("%c", ch);
       if(ch == '\n')
	  break;
   
       cmdl[pos++] = ch;
   
   }  // printf("%d %d \n",cmdline[pos], pos);

//these two lines are added, in case user press ctrl+d to exit the shell
   if(ch == EOF && pos == 0)
      return NULL;
   return cmdl;
}


